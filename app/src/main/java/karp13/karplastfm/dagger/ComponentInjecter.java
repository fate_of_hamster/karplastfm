package karp13.karplastfm.dagger;

import android.content.Context;

public class ComponentInjecter {

    private static ComponentApp componentApp;

    public static void initComponent(Context context) {
        ModuleStorage moduleStorage = new ModuleStorage();
        ModuleAndroid moduleAndroid = new ModuleAndroid(context);
        componentApp = DaggerComponentApp
                .builder()
                .moduleStorage(moduleStorage)
                .moduleAndroid(moduleAndroid)
                .build();
    }

    public static ComponentApp getComponent() {
       if (componentApp == null)
            throw new NullPointerException("ComponentInjecter: ComponentApp не инициализирован"); // хм, не знаю, нужно ли это тут
        return componentApp;
    }
}
