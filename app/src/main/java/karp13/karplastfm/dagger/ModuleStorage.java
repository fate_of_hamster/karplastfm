package karp13.karplastfm.dagger;

import android.content.Context;

import javax.inject.Singleton;
import dagger.Module;
import dagger.Provides;
import karp13.karplastfm.server_client.ServerClient;

@Module
public class ModuleStorage {

    @Provides
    @Singleton
    ServerClient providesServerClient(Context context) {
        return new ServerClient(context);
    }
}
