package karp13.karplastfm.dagger;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/*
Сейчас этот модуль будет поставлять нам контекст
*/

@Module
public class ModuleAndroid {
    Context context;
    public ModuleAndroid(Context context) {
        this.context = context;
    }

    @Provides
    @Singleton
    Context providesContext() {
        return context;
    }
}
