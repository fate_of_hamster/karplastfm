package karp13.karplastfm.dagger;

import javax.inject.Singleton;
import dagger.Component;
import karp13.karplastfm.MyApp;
import karp13.karplastfm.screens.album_screen.AlbumActivityPresenter;
import karp13.karplastfm.screens.main_screen.MainActivityPresenter;
import karp13.karplastfm.server_client.ServerClient;

@Singleton
@Component(modules = {
        ModuleStorage.class,
        ModuleAndroid.class})
public interface ComponentApp {
    void inject(MyApp object);
    void inject(ServerClient object);
    void inject(MainActivityPresenter object);
    void inject(AlbumActivityPresenter object);
}
