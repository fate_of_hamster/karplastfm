package karp13.karplastfm.screens.album_screen;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.hannesdorfmann.mosby3.mvp.MvpActivity;
import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;
import karp13.karplastfm.R;
import karp13.karplastfm.data.Track;

public class AlbumActivity extends MvpActivity<AlbumActivityView, AlbumActivityPresenter> implements AlbumActivityView  {

    private static final String EXTRA_ALBUM = "karp13.karplastfm.album.album";
    private static final String EXTRA_ARTIST = "karp13.karplastfm.album.artist";
    private static final String EXTRA_MBID = "karp13.karplastfm.album.mbid";

    @BindView(R.id.photo_layout) public LinearLayout photoLayout;
    @BindView(R.id.img_photo) public ImageView imgPhoto;
    @BindView(R.id.album_layout) public LinearLayout albumLayout;
    @BindView(R.id.lbl_album) public TextView lblAlbum;
    @BindView(R.id.artist_layout) public LinearLayout artistLayout;
    @BindView(R.id.lbl_artist) public TextView lblArtist;
    @BindView(R.id.tracks_layout) public LinearLayout tracksLayout;
    @BindView(R.id.track_list) public RecyclerView trackList;
    @BindView(R.id.info_layout) public LinearLayout infoLayout;
    @BindView(R.id.lbl_info) public TextView lblInfo;
    @BindView(R.id.swipe_refresh) public SwipeRefreshLayout swipeRefresh;

    private TracksAdapter adapter;
    private LinearLayoutManager linearLayoutManager;

    public static void start(Context context, String album, String artist, String mbid) {
        Intent intent = new Intent(context, AlbumActivity.class);
        intent.putExtra(EXTRA_ALBUM, album);
        intent.putExtra(EXTRA_ARTIST, artist);
        intent.putExtra(EXTRA_MBID, mbid);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String album  = null;
        String artist = null;
        String mbid   = null;

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            album  = bundle.getString(EXTRA_ALBUM, null);
            artist = bundle.getString(EXTRA_ARTIST, null);
            mbid   = bundle.getString(EXTRA_MBID, null);
        }

        if ((album != null)
                || (artist != null)
                || (mbid != null)) {
            setContentView(R.layout.activity_album);
            ButterKnife.bind(this);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
            swipeRefresh.setEnabled(false);
            trackList.setHasFixedSize(true);
            linearLayoutManager = new LinearLayoutManager(this);
            trackList.setLayoutManager(linearLayoutManager);
            adapter = new TracksAdapter();
            trackList.setAdapter(adapter);
            DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(trackList.getContext(), linearLayoutManager.getOrientation());
            trackList.addItemDecoration(dividerItemDecoration);
            photoLayout.setVisibility(View.GONE);
            albumLayout.setVisibility(View.GONE);
            artistLayout.setVisibility(View.GONE);
            tracksLayout.setVisibility(View.GONE);
            infoLayout.setVisibility(View.GONE);
            presenter.startAlbumGetInfo(album, artist, mbid);
        }
        else {
            showMessage(getString(R.string.error_album_open));
            finish();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                presenter.doLoadingStop();
                this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @NonNull
    @Override
    public AlbumActivityPresenter createPresenter() {
        return new AlbumActivityPresenter();
    }

    @Override
    public void showLoadingStart() {
        swipeRefresh.setRefreshing(true);
    }

    @Override
    public void showLoadingStop() {
        swipeRefresh.setRefreshing(false);
    }

    @Override
    public void showMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void setData(String album, String artist, String photoLink, String info, List<Track> tracks) {
        getSupportActionBar().setTitle(album);
        getSupportActionBar().setSubtitle(artist);

        if (album != null) {
            lblAlbum.setText(album);
            albumLayout.setVisibility(View.VISIBLE);
        }
        else
            albumLayout.setVisibility(View.GONE);

        if (artist != null) {
            lblArtist.setText(artist);
            artistLayout.setVisibility(View.VISIBLE);
        }
        else
            artistLayout.setVisibility(View.GONE);

        if (info != null) {
            lblInfo.setText(Html.fromHtml(info.replace("\n", "<br>")));
            infoLayout.setVisibility(View.VISIBLE);
        }
        else
            infoLayout.setVisibility(View.GONE);

        if (photoLink != null) {
            photoLayout.setVisibility(View.VISIBLE);
            Glide.with(this)
                    .load(photoLink)
                    .placeholder(R.drawable.loading_ring)
                    .error(R.drawable.ic_folder)
                    .into(new SimpleTarget<GlideDrawable>() {
                        @Override
                        public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> glideAnimation) {
                            imgPhoto.setImageDrawable(resource);
                        }

                        @Override
                        public void onLoadFailed(Exception e, Drawable errorDrawable) {
                            super.onLoadFailed(e, errorDrawable);
                            imgPhoto.setVisibility(View.GONE);
                        }
                    });
        }
        else
            photoLayout.setVisibility(View.GONE);

        if ((tracks != null)
                && (tracks.size() > 0)) {
            tracksLayout.setVisibility(View.VISIBLE);
            adapter.setTraks(tracks);
        }
        else
            tracksLayout.setVisibility(View.GONE);
    }
}
