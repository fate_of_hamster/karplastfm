package karp13.karplastfm.screens.album_screen;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;
import karp13.karplastfm.R;
import karp13.karplastfm.data.Track;

public class TracksAdapter extends RecyclerView.Adapter<TracksAdapter.CustomViewHolder> {

    private List<Track> tracks;

    public void setTraks(List<Track> tracks) {
        this.tracks = tracks;
        this.notifyDataSetChanged();
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.track_item, parent, false);
        return new CustomViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CustomViewHolder holder, int position) {
        Track track = tracks.get(position);
        if (track != null) {
            holder.lblName.setText(track.getName());
            holder.lblDuration.setText(track.getDurationFormat());
        }
    }

    @Override
    public int getItemCount() {
        return (tracks != null) ? tracks.size() : 0;
    }

    public static class CustomViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.lbl_name) public TextView lblName;
        @BindView(R.id.lbl_duration) public TextView lblDuration;
        public CustomViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}