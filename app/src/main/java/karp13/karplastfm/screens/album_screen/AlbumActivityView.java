package karp13.karplastfm.screens.album_screen;

import com.hannesdorfmann.mosby3.mvp.MvpView;
import java.util.List;
import karp13.karplastfm.data.Track;

public interface AlbumActivityView extends MvpView {
    void showLoadingStart();
    void showLoadingStop();
    void showMessage(String message);
    void setData(String album, String artist, String photoLink, String wiki, List<Track> tracks);
}
