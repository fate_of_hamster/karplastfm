package karp13.karplastfm.screens.album_screen;

import android.content.Context;
import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;
import javax.inject.Inject;
import karp13.karplastfm.R;
import karp13.karplastfm.dagger.ComponentInjecter;
import karp13.karplastfm.data.Album;
import karp13.karplastfm.screens.main_screen.ResultForView;
import karp13.karplastfm.server_client.ServerClient;
import karp13.karplastfm.server_client.responce.AlbumGetInfoResponce;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AlbumActivityPresenter extends MvpBasePresenter<AlbumActivityView> {

    @Inject public ServerClient serverClient;
    @Inject public Context context;
    private Call<AlbumGetInfoResponce> callAlbumGetInfo;
    private boolean isLoadingInProcess = false;

    private ResultForView bufReslutForView;


    public AlbumActivityPresenter() {
        super();
        ComponentInjecter.getComponent().inject(this);
    }

    @Override
    public void attachView(AlbumActivityView view) {
        super.attachView(view);
        if (bufReslutForView != null) {
            setResultToView(bufReslutForView);
            bufReslutForView = null;
        }
    }

    public void setResultToView(ResultForView resultForView) {
        onLoadingStop();
        if (isViewAttached()) {
            if (resultForView.getErrorMessage() != null)
                getView().showMessage(resultForView.getErrorMessage());

            AlbumGetInfoResponce result = (AlbumGetInfoResponce) resultForView.getResult();
            if (result != null) {
                Album album = result.getAlbum();
                if (album != null) {
                    getView().setData(
                            album.getName(),
                            album.getArtist(),
                            album.getLinkOnBigImage(),
                            (album.getWiki() != null) ? album.getWiki().getContent() : null,
                            (album.getTracks() != null) ? album.getTracks().getTrack() : null);
                }
            }
        } else
            this.bufReslutForView = resultForView;
    }

    private void onLoadingStart() {
        if (!isLoadingInProcess) {
            isLoadingInProcess = true;
            if (isViewAttached())
                getView().showLoadingStart();
        }
    }

    private void onLoadingStop() {
        if (isLoadingInProcess) {
            isLoadingInProcess = false;
            if (isViewAttached())
                getView().showLoadingStop();
        }
    }

    public void doLoadingStop() {
        if (callAlbumGetInfo != null) {
            callAlbumGetInfo.cancel();
            callAlbumGetInfo = null;
        }
        onLoadingStop();
    }


    //-------------------------------------------------------------------------------- поиск альбома
    public void startAlbumGetInfo(String album, String artist, String mbid) {
        if (isLoadingInProcess)
            doLoadingStop();

        Album cacheAlbum = Album.cacheGet(album, artist, mbid);

        if (cacheAlbum != null) {
            Album.cacheUpdateDate(cacheAlbum);
            setResultToView(new ResultForView(null, new AlbumGetInfoResponce(cacheAlbum)));
        }
        else {
            startGetAlbumFromServer(album, artist, mbid);
        }
    }

    private void startGetAlbumFromServer(String album, String artist, String mbid) {
        String errorMessage = serverClient.checkOnline();
        if (errorMessage != null)
            setResultToView(new ResultForView(errorMessage, null));
        else {
            onLoadingStart();
            callAlbumGetInfo = serverClient.getServerApi().AlbumGetInfo(album, artist, mbid, ServerClient.LANG_RU, ServerClient.API_KEY, ServerClient.FORMAT_JSON);
            callAlbumGetInfo.enqueue(new albumGetInfoCallback());
        }
    }

    private class albumGetInfoCallback implements Callback<AlbumGetInfoResponce> {
        @Override
        public void onResponse(Call<AlbumGetInfoResponce> call, Response<AlbumGetInfoResponce> response) {
            String errorMessage = serverClient.checkServerResponse(response);

            if (errorMessage != null) {
                setResultToView(new ResultForView(errorMessage, null));
            }
            else {
                AlbumGetInfoResponce albumGetInfoResponce = response.body();
                if (albumGetInfoResponce == null) {
                    setResultToView(new ResultForView(context.getString(R.string.error_response_data_null), null));
                }
                else if (albumGetInfoResponce.getError() != null) {
                    setResultToView(new ResultForView(albumGetInfoResponce.getMessage(), null));
                }
                else {
                    if (albumGetInfoResponce.getAlbum() != null)
                        Album.cacheSave(albumGetInfoResponce.getAlbum());

                    setResultToView(new ResultForView(null, albumGetInfoResponce));
                }
            }

            callAlbumGetInfo = null;
        }
        @Override
        public void onFailure(Call<AlbumGetInfoResponce> call, Throwable t) {
            setResultToView(new ResultForView(t.getMessage(), null));
            callAlbumGetInfo = null;
        }
    }
}
