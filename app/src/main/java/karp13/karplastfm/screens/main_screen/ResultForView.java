package karp13.karplastfm.screens.main_screen;

public class ResultForView {

    private String errorMessage;

    private Object result;

    public ResultForView(String errorMessage, Object result) {
        this.errorMessage = errorMessage;
        this.result = result;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public Object getResult() {
        return result;
    }
}
