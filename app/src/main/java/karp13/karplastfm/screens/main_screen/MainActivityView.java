package karp13.karplastfm.screens.main_screen;

import com.hannesdorfmann.mosby3.mvp.MvpView;
import java.util.List;
import karp13.karplastfm.data.AlbumSimple;

public interface MainActivityView extends MvpView {

    void showLoadingStart();
    void showLoadingStop();
    void showMessage(String message);
    void setTotalCount(String totalCount);
    void setAlbums(List<AlbumSimple> albums);
}
