package karp13.karplastfm.screens.main_screen;

public interface AlbumClickListener {
    void onAlbumClick(int position);
}
