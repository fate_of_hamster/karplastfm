package karp13.karplastfm.screens.main_screen;

import android.content.Context;
import android.support.annotation.NonNull;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.hannesdorfmann.mosby3.mvp.MvpActivity;
import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import karp13.karplastfm.R;
import karp13.karplastfm.data.AlbumSimple;
import karp13.karplastfm.screens.album_screen.AlbumActivity;

public class MainActivity extends MvpActivity<MainActivityView, MainActivityPresenter> implements MainActivityView, AlbumClickListener  {

    @BindView(R.id.edt_search) public EditText edtSearch;
    @BindView(R.id.swipe_refresh) public SwipeRefreshLayout swipeRefresh;
    @BindView(R.id.albums_list) public RecyclerView albumsList;
    @BindView(R.id.search_total_count_layout) public RelativeLayout searchPanel;
    @BindView(R.id.lbl_total_count) public TextView searchTotalCount;
    private AlbumSimpleAdapter adapter;
    private LinearLayoutManager layoutManager;
    private int lastVisiblePosWhenLoad = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        swipeRefresh.setEnabled(false);
        searchPanel.setVisibility(View.GONE);
        albumsList.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        albumsList.setLayoutManager(layoutManager);
        adapter = new AlbumSimpleAdapter(this, this);
        albumsList.setAdapter(adapter);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(albumsList.getContext(), layoutManager.getOrientation());
        albumsList.addItemDecoration(dividerItemDecoration);
        RecyclerView.OnScrollListener mScrollListener = new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (!presenter.isLoadingInProcess()) {
                    int visibleCount   = layoutManager.getChildCount();
                    int lastVisiblePos = layoutManager.findFirstVisibleItemPosition();
                    int total          = layoutManager.getItemCount();
                    if ((lastVisiblePos + visibleCount >= total)
                            && (lastVisiblePosWhenLoad != lastVisiblePos)) {
                        lastVisiblePosWhenLoad = lastVisiblePos;
                        presenter.onScrollToEnd();
                    }
                }
            }
        };
        albumsList.addOnScrollListener(mScrollListener);
        edtSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE)
                    doSearch();
                return false;
            }
        });

        if (presenter.getNowVisibleAlbums() != null)
            setAlbums(presenter.getNowVisibleAlbums());
    }

    @NonNull
    @Override
    public MainActivityPresenter createPresenter() {
        return new MainActivityPresenter();
    }

    @OnClick(R.id.btn_search)
    public void doSearch() {
        String searchString = edtSearch.getText().toString();
        if (!searchString.isEmpty()) {
            hideKeyBoard();
            presenter.startSearchAlbum(searchString, 1);
        }
    }


    @Override
    public void showLoadingStart() {
        swipeRefresh.setRefreshing(true);
    }

    @Override
    public void showLoadingStop() {
        swipeRefresh.setRefreshing(false);
    }

    @Override
    public void showMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void setTotalCount(String totalCount) {
        if (totalCount != null) {
            searchPanel.setVisibility(View.VISIBLE);
            searchTotalCount.setText(totalCount);
        }
        else
            searchPanel.setVisibility(View.GONE);
    }

    @Override
    public void setAlbums(List<AlbumSimple> albums) {
        if (adapter != null)
            adapter.setAlbums(albums);
    }

    @Override
    public void onAlbumClick(int position) {
        if (adapter != null) {
            AlbumSimple album = adapter.getAlbum(position);
            AlbumActivity.start(this, album.getName(), album.getArtist(), album.getMbid());
        }
    }

    private void hideKeyBoard() {
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
    }
}
