package karp13.karplastfm.screens.main_screen;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;
import karp13.karplastfm.R;
import karp13.karplastfm.data.AlbumSimple;

public class AlbumSimpleAdapter extends RecyclerView.Adapter<AlbumSimpleAdapter.CustomViewHolder> {

    private List<AlbumSimple> albums;
    private AlbumClickListener albumClickListener;
    private Context context;

    public AlbumSimpleAdapter(Context context, AlbumClickListener albumClickListener) {
        this.albumClickListener = albumClickListener;
        this.context = context;
    }

    public void setAlbums(List<AlbumSimple> newAlbums) {
        albums = newAlbums;
        notifyDataSetChanged();
    }

    public AlbumSimple getAlbum(int pos) {
        if ((albums != null)
                && (albums.size() > 0)
                && (pos >= 0)
                && (pos < albums.size()))
            return albums.get(pos);
        else
            return null;
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.album_item, parent, false);
        return new CustomViewHolder(view, albumClickListener);
    }

    @Override
    public void onBindViewHolder(CustomViewHolder holder, int position) {
        AlbumSimple album = albums.get(position);
        if (album != null) {
            holder.lblAlbum.setText(album.getName());
            holder.lblArtist.setText(album.getArtist());
            Glide.with(context)
                    .load(album.getLinkOnSmallImage())
                    .placeholder(R.drawable.loading_ring)
                    .error(R.drawable.ic_folder)
                    .centerCrop()
                    .into(holder.imgPhoto);
        }
    }

    @Override
    public int getItemCount() {
        return (albums != null) ? albums.size() : 0;
    }

    public static class CustomViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.img_photo) public ImageView imgPhoto;
        @BindView(R.id.lbl_album) public TextView lblAlbum;
        @BindView(R.id.lbl_artist) public TextView lblArtist;
        public CustomViewHolder(View view, AlbumClickListener albumClickListener) {
            super(view);
            ButterKnife.bind(this, view);
            setListener(albumClickListener);
        }
        private void setListener(final AlbumClickListener albumClickListener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (albumClickListener != null)
                        albumClickListener.onAlbumClick(getAdapterPosition());
                }
            });
        }
    }
}