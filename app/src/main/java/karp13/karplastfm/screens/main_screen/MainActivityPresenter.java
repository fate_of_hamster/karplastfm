package karp13.karplastfm.screens.main_screen;

import android.content.Context;
import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;
import java.util.List;
import javax.inject.Inject;
import karp13.karplastfm.R;
import karp13.karplastfm.dagger.ComponentInjecter;
import karp13.karplastfm.data.AlbumSimple;
import karp13.karplastfm.server_client.ServerClient;
import karp13.karplastfm.server_client.responce.AlbumSearchResponce;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivityPresenter extends MvpBasePresenter<MainActivityView> {

    @Inject public ServerClient serverClient;
    @Inject public Context context;

    private boolean isLoadingInProcess = false;
    private ResultForView bufReslutForView;
    private final int ITEMS_PER_PAGE = 30;
    private int nowPageCount = 0;
    private String searchString = null;
    private List<AlbumSimple> nowVisibleAlbums = null;


    public MainActivityPresenter() {
        super();
        ComponentInjecter.getComponent().inject(this);
    }

    @Override
    public void attachView(MainActivityView view) {
        super.attachView(view);
        if (bufReslutForView != null) {
            setResultToView(bufReslutForView);
            bufReslutForView = null;
        }
    }

    public void setResultToView(ResultForView resultForView) {
        onLoadingStop();
        if (isViewAttached()) {
            if (resultForView.getErrorMessage() != null)
                getView().showMessage(resultForView.getErrorMessage());

            AlbumSearchResponce albumSearchResponce = (AlbumSearchResponce) resultForView.getResult();
            if (albumSearchResponce != null) {
                if (albumSearchResponce.getResults().getOpenSearchQuery().getStartPage() == 1) {
                    getView().setTotalCount(albumSearchResponce.getResults().getTotalResults());
                    nowVisibleAlbums = albumSearchResponce.getResults().getAlbumMatches().getAlbums();
                }
                else if (nowVisibleAlbums != null)
                    nowVisibleAlbums.addAll(albumSearchResponce.getResults().getAlbumMatches().getAlbums());

                getView().setAlbums(nowVisibleAlbums);
            }
        } else
            this.bufReslutForView = resultForView;
    }

    public List<AlbumSimple> getNowVisibleAlbums() {
        return nowVisibleAlbums;
    }

    private void onLoadingStart() {
        if (!isLoadingInProcess) {
            isLoadingInProcess = true;
            if (isViewAttached())
                getView().showLoadingStart();
        }
    }

    private void onLoadingStop() {
        if (isLoadingInProcess) {
            isLoadingInProcess = false;
            if (isViewAttached())
                getView().showLoadingStop();
        }
    }

    public void onScrollToEnd() {
        startSearchAlbum(searchString, nowPageCount + 1);
    }

    public boolean isLoadingInProcess() {
        return isLoadingInProcess;
    }

    //-------------------------------------------------------------------------------- поиск альбома
    public void startSearchAlbum(String album, int page) {
        if (!isLoadingInProcess) {
            showSearchResponceFromCache(album, page); // сначала показываю данные из кэша
            startSearchAlbumFromServer(album, page);  // и потом сразу пробую подгрузить данные с сервера, что бы затем обновить результаты более свежими
        }
    }

    public void showSearchResponceFromCache(String album, int page) {
        AlbumSearchResponce albumSearchResponce = AlbumSearchResponce.cacheGet(album, page);
        if (albumSearchResponce != null)
            setResultToView(new ResultForView(null, albumSearchResponce));
    }

    private void startSearchAlbumFromServer(String album, int page) {
        searchString = album;
        nowPageCount = page;
        String errorMessage = serverClient.checkOnline();
        if (errorMessage != null)
            setResultToView(new ResultForView(errorMessage, null));
        else {
            onLoadingStart();
            serverClient.getServerApi()
                    .AlbumSearch(searchString, ITEMS_PER_PAGE, nowPageCount, ServerClient.API_KEY, ServerClient.FORMAT_JSON)
                    .enqueue(new SearchAlbumCallback());
        }
    }

    private class SearchAlbumCallback implements Callback<AlbumSearchResponce> {
        @Override
        public void onResponse(Call<AlbumSearchResponce> call, Response<AlbumSearchResponce> response) {
            String errorMessage = serverClient.checkServerResponse(response);

            if (errorMessage != null) {
                setResultToView(new ResultForView(errorMessage, null));
            }
            else {
                AlbumSearchResponce albumSearchResponce = response.body();
                if (albumSearchResponce == null) {
                    setResultToView(new ResultForView(context.getString(R.string.error_response_data_null), null));
                }
                else if (albumSearchResponce.getError() != null) {
                    setResultToView(new ResultForView(albumSearchResponce.getMessage(), null));
                }
                else {
                    AlbumSearchResponce.cacheSave(searchString, nowPageCount, albumSearchResponce); // сохраняем в кэш
                    setResultToView(new ResultForView(null, albumSearchResponce));
                }
            }
        }
        @Override
        public void onFailure(Call<AlbumSearchResponce> call, Throwable t) {
            setResultToView(new ResultForView(t.getMessage(), null));
        }
    }
}
