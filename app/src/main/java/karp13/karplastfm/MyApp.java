package karp13.karplastfm;

import android.app.Application;
import javax.inject.Inject;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import karp13.karplastfm.dagger.ComponentInjecter;
import karp13.karplastfm.data.Album;
import karp13.karplastfm.server_client.ServerClient;
import karp13.karplastfm.server_client.responce.AlbumSearchResponce;

public class MyApp extends Application {

    public static final String RREALM_DB_NAME = "karp13.karplastfm.localDB";

    @Inject
    public ServerClient serverClient;

    @Override
    public void onCreate() {
        super.onCreate();

        // Dagger
        ComponentInjecter.initComponent(getApplicationContext());
        ComponentInjecter.getComponent().inject(this);

        // RetroFit
        serverClient.init();

        // Realm
        Realm.init(getApplicationContext());
        RealmConfiguration config = new RealmConfiguration.Builder().name(RREALM_DB_NAME).build();
        Realm.setDefaultConfiguration(config);

        // Realm: очистка кэша (примечание: не люблю статику, но пока не заморачиваюсь с вынесением этих операций в отдельный синглтон RealmManager
        AlbumSearchResponce.cacheClear();
        Album.cacheClear();
    }
}
