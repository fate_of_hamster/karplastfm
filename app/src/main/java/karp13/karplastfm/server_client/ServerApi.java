package karp13.karplastfm.server_client;

import karp13.karplastfm.server_client.responce.AlbumGetInfoResponce;
import karp13.karplastfm.server_client.responce.AlbumSearchResponce;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ServerApi {

    String BASE_URL       = "http://ws.audioscrobbler.com/2.0/";
    String PREFIX_METHOD  = "?method=";
    String ALBUM_SEARCH   = PREFIX_METHOD + "album.search";
    String ALBUM_GET_INFO = PREFIX_METHOD + "album.getinfo";

    // поиск альбома по названию
    @GET(ALBUM_SEARCH)
    Call<AlbumSearchResponce> AlbumSearch(
            @Query("album") String album,      // наименование альбома
            @Query("limit") Integer limit,     // количество результатов на странице
            @Query("page") Integer page,       // страница
            @Query("api_key") String api_key,  // ключ lost.fm
            @Query("format") String format     // формат ответа
    );

    // информация об альбоме
    @GET(ALBUM_GET_INFO)
    Call<AlbumGetInfoResponce> AlbumGetInfo(
            @Query("album") String album,      // альбом
            @Query("artist") String artist,    // исполнитель
            @Query("mbid") String mbid,        // индентификатор альбома с musicbrainz (его может не быть)
            @Query("lang") String lang,        // язык перевода для описания альбома
            @Query("api_key") String api_key,  // ключ lost.fm
            @Query("format") String format     // формат ответа
    );

}