package karp13.karplastfm.server_client;

import android.content.Context;
import android.net.ConnectivityManager;

import karp13.karplastfm.BuildConfig;
import karp13.karplastfm.R;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ServerClient {

    public static final String API_KEY = "67b350cf0f53496cd7d793fd4b000b94";
    public static final String FORMAT_JSON = "json";
    public static final String LANG_RU = "ru";

    private ServerApi serverApi;
    private Context context;

    public ServerClient(Context context) {
        this.context = context;
    }

    public ServerApi getServerApi() {
        return serverApi;
    }

    // инициализация
    public void init() {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

        if (BuildConfig.DEBUG)
            addInterceptorLogging(httpClient);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ServerApi.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient.build())
                .build();

        serverApi = retrofit.create(ServerApi.class);
    }

    // интерсептор для вывода информации в лог
    private void addInterceptorLogging(OkHttpClient.Builder httpClient) {
        HttpLoggingInterceptor loggingBody = new HttpLoggingInterceptor();
        loggingBody.setLevel(HttpLoggingInterceptor.Level.BODY);
        httpClient.addInterceptor(loggingBody);
    }

    // проверка связи с сетью
    public String checkOnline() {
        return (isOnline()) ? null : context.getString(R.string.error_offline);
    }

    private boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return (cm.getActiveNetworkInfo() != null)
                && cm.getActiveNetworkInfo().isConnectedOrConnecting();
    }

    // проверка ответа от сервера
    public String checkServerResponse(Response response) {
        if (response == null)
            return context.getString(R.string.error_response_from_server_empty);
        else if (!response.isSuccessful())
            return context.getString(R.string.error_response_from_server);
        else if (response.body() == null)
            return context.getString(R.string.error_response_body_null);
        else
            return null;
    }
}
