package karp13.karplastfm.server_client.responce;

import com.google.gson.annotations.SerializedName;
import karp13.karplastfm.data.Album;

public class AlbumGetInfoResponce {

    @SerializedName("album")
    private Album album;

    @SerializedName("message")
    private String message;

    @SerializedName("error")
    private Integer error;

    public AlbumGetInfoResponce(Album album) {
        this.album = album;
    }

    public String getMessage() {
        return message;
    }

    public Integer getError() {
        return error;
    }

    public Album getAlbum() {
        return album;
    }
}
