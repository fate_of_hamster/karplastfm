package karp13.karplastfm.server_client.responce;

import com.google.gson.annotations.SerializedName;
import java.util.Calendar;
import java.util.Date;
import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmResults;
import io.realm.Sort;
import io.realm.annotations.Index;
import karp13.karplastfm.data.Results;

public class AlbumSearchResponce extends RealmObject {

    public static final String FIELD_SEARCH_STRING = "searchString";
    public static final String FIELD_LOCAL_CACHE_DATE = "localCacheDate";
    public static final int LIMIT_CACHE_COUNT = 10; // количество последних запросов, которые будем кэшировать
    public static final int LIMIT_CACHE_DAYS_COUNT = 30; // количество дней, которое хранятся закэшированные данные с момента их кэширования (что бы не хранить запросы, которые пользователь делал, например, год или десять лет назад)

    @Index
    @SerializedName("searchString")
    private String searchString;

    @Index
    @SerializedName(FIELD_LOCAL_CACHE_DATE)
    private Date localCacheDate;

    @SerializedName("results")
    private Results results;

    @SerializedName("message")
    private String message;

    @SerializedName("error")
    private Integer error;

    public String getMessage() {
        return message;
    }

    public Integer getError() {
        return error;
    }

    public Results getResults() {
        return results;
    }

    public String getSearchString() {
        return searchString;
    }

    public void setSearchString(String searchString) {
        this.searchString = searchString;
    }

    public Date getLocalCacheDate() {
        return localCacheDate;
    }

    public void updateLocalCacheDate() {
        localCacheDate = new Date();
    }

    public static AlbumSearchResponce cacheGet(String searchString, int page) {
        if (page == 1) // кэшируем и показываем только запросы первой страницы!
            return cacheGet(Realm.getDefaultInstance(), searchString);
        else
            return null;
    }

    public static AlbumSearchResponce cacheGet(Realm realm, String searchString) {
        searchString = (searchString.equals("")) ? null : searchString;
        if ((realm != null)
                && (searchString != null)) { // исхожу из того, что что-то из этого точно должно быть не null
            return realm.where(AlbumSearchResponce.class)
                    .equalTo(FIELD_SEARCH_STRING, searchString)
                    .findFirst();
        } else
            return null;
    }

    public static void cacheSave(String searchString, int page, AlbumSearchResponce responce) {
        if ((responce != null)
                && (page == 1)
                && (responce.getResults().getOpenSearchQuery().getStartPage() == 1)) { // кэшируем и показываем только запросы первой страницы!
            Realm realm = Realm.getDefaultInstance();
            AlbumSearchResponce existeResponce = cacheGet(realm, searchString);

            if (existeResponce == null) { // если ещё нет, то добавляем в кэш
                cacheClear(true);
                realm.beginTransaction();
                responce.setSearchString(searchString);
                responce.updateLocalCacheDate();
                realm.insert(responce);
                realm.commitTransaction();
            }
        }
    }


    public static void cacheClear() {
        cacheClear(false);
    }
    public static void cacheClear(boolean isForNewInsert) {
        int limitCount = (isForNewInsert) ? LIMIT_CACHE_COUNT - 1 : LIMIT_CACHE_COUNT;

        Realm realm = Realm.getDefaultInstance();
        final RealmResults<AlbumSearchResponce> results = realm
                .where(AlbumSearchResponce.class)
                .findAllSorted(FIELD_LOCAL_CACHE_DATE, Sort.DESCENDING);

        if (results.size() > limitCount) {
            // дата кэширования последнего элемента, который можно кэшировать
            Date limitDateByCount = null;
            if (limitCount > 0)
                limitDateByCount = results.get(limitCount - 1).getLocalCacheDate();

            // дата хранения кэша
            Date limitDate = new Date();
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(limitDate);
            calendar.add(Calendar.DATE, - LIMIT_CACHE_DAYS_COUNT);
            limitDate = calendar.getTime();

            // определяем дату, с которой нужно удалить кэш
            if ((limitDateByCount != null)
                    && (limitDateByCount.getTime() > limitDate.getTime()))
                limitDate = limitDateByCount;

            // удаляем не нужный кэш
            final RealmResults<AlbumSearchResponce> delResults = realm
                    .where(AlbumSearchResponce.class)
                    .lessThan(FIELD_LOCAL_CACHE_DATE, limitDate)
                    .findAll();
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    delResults.deleteAllFromRealm();
                }
            });
        }
    }
}
