package karp13.karplastfm.data;

import com.google.gson.annotations.SerializedName;
import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;

public class Tracks extends RealmObject {

    @SerializedName("track")
    private RealmList<Track> track;

    public RealmList<Track> getTrack() {
        return track;
    }
}
