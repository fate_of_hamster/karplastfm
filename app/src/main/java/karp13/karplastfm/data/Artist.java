package karp13.karplastfm.data;

import com.google.gson.annotations.SerializedName;
import io.realm.RealmObject;

public class Artist  extends RealmObject {

    @SerializedName("mbid")
    private String mbid;

    @SerializedName("name")
    private String name;

    @SerializedName("url")
    private String url;

    public String getMbid() {
        return mbid;
    }

    public String getName() {
        return name;
    }

    public String getUrl() {
        return url;
    }
}
