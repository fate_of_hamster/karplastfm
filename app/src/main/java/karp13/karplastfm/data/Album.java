package karp13.karplastfm.data;

import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.RealmResults;
import io.realm.annotations.Index;

public class Album extends RealmObject {

    private static final String FIELD_NAME = "name";
    private static final String FIELD_ARTIST = "artist";
    private static final String FIELD_MBID = "mbid";
    private static final String FIELD_LOCAL_CACHE_DATE = "localCacheDate";
    public static final int LIMIT_CACHE_DAYS_COUNT = 30; // количество дней, которое хранятся закэшированные данные с момента их кэширования


    @Index
    @SerializedName(FIELD_NAME)
    private String name;

    @Index
    @SerializedName(FIELD_ARTIST)
    private String artist;

    @Index
    @SerializedName(FIELD_MBID)
    private String mbid;

    @SerializedName("url")
    private String url;

    @SerializedName("image")
    private RealmList<Image> images;

    @SerializedName("tracks")
    private Tracks tracks;

    @SerializedName("wiki")
    private Wiki wiki;

    @SerializedName(FIELD_LOCAL_CACHE_DATE)
    private Date localCacheDate;



    public static Album cacheGet(String album, String artist, String mbid) {
        return cacheGet(Realm.getDefaultInstance(), album, artist, mbid);
    }
    public static Album cacheGet(Realm realm, String album, String artist, String mbid) {
        if (album != null)
            album = (album.equals("")) ? null : album;
        if (artist != null)
            artist = (artist.equals("")) ? null : artist;
        if (mbid != null)
            mbid = (mbid.equals("")) ? null : mbid;

        if ((realm != null)
                && (   (album != null)
                    || (artist != null)
                    || (mbid != null))) { // исхожу из того, что что-то из этого точно должно быть не null
            return realm.where(Album.class)
                    .equalTo(FIELD_NAME, album)
                    .equalTo(FIELD_ARTIST, artist)
                    .equalTo(FIELD_MBID, mbid)
                    .findFirst();
        }
        else
            return null;
    }

    public static void cacheSave(Album album) {
        if (album != null) {
            Realm realm = Realm.getDefaultInstance();
            Album existeAlbum = cacheGet(realm, album.getName(), album.getArtist(), album.getMbid());
            if (existeAlbum == null) { // если ещё нет, то добавляем в кэш
                realm.beginTransaction();
                album.updateLocalCacheDate();
                realm.insert(album);
                realm.commitTransaction();
            }
        }
    }

    public static void cacheUpdateDate(Album album) {
        if (album != null) {
            Realm realm = Realm.getDefaultInstance();
            realm.beginTransaction();
            album.updateLocalCacheDate();
            realm.commitTransaction();
        }
    }

    public static void cacheClear() {
        Date limitDate = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(limitDate);
        calendar.add(Calendar.DATE, - LIMIT_CACHE_DAYS_COUNT);
        limitDate = calendar.getTime();

        Realm realm = Realm.getDefaultInstance();
        final RealmResults<Album> results = realm
                .where(Album.class)
                .lessThan(FIELD_LOCAL_CACHE_DATE, limitDate)
                .findAll();

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                results.deleteAllFromRealm();
            }
        });
    }


    public void updateLocalCacheDate() {
        localCacheDate = new Date();
    }

    public String getMbid() {
        return mbid;
    }

    public String getName() {
        return name;
    }

    public String getArtist() {
        return artist;
    }

    public String getUrl() {
        return url;
    }

    public List<Image> getImages() {
        return images;
    }

    public Tracks getTracks() {
        return tracks;
    }

    public Wiki getWiki() {
        return wiki;
    }

    @Override
    public String toString() {
        return new GsonBuilder().create().toJson(this, Album.class);
    }

    public String getLinkOnBigImage() {
        if (images != null) {
            String small = null;
            String medium = null;
            String large = null;
            String extraLarge = null;
            String mega = null;

            for (Image image : images) {
                if (image.isSmall())
                    small = image.getText();
                else if (image.isMedium())
                    medium = image.getText();
                else if (image.isLarge())
                    large = image.getText();
                else if (image.isExtraLarge())
                    extraLarge = image.getText();
                else if (image.isMega())
                    mega = image.getText();
            }

            return (mega != null) ? mega :
                    (extraLarge != null) ? extraLarge :
                            (large != null) ? large :
                                    (medium != null) ? medium : small;
        }
        else
            return null;
    }
}
