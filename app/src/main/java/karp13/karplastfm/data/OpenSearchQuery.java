package karp13.karplastfm.data;

import com.google.gson.annotations.SerializedName;
import io.realm.RealmObject;


public class OpenSearchQuery extends RealmObject {

    @SerializedName("searchTerms")
    private String searchTerms;

    @SerializedName("startPage")
    private String startPage;


    public String getSearchTerms() {
        return searchTerms;
    }

    public int getStartPage() {
        return Integer.parseInt(startPage);
    }

}
