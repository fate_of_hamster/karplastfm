package karp13.karplastfm.data;

import com.google.gson.annotations.SerializedName;
import io.realm.RealmObject;

public class Wiki extends RealmObject {
    @SerializedName("published")
    private String published;

    @SerializedName("summary")
    private String summary;

    @SerializedName("content")
    private String content;

    public String getPublished() {
        return published;
    }

    public String getSummary() {
        return summary;
    }

    public String getContent() {
        return content;
    }

}
