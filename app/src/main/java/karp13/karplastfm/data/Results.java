package karp13.karplastfm.data;

import com.google.gson.annotations.SerializedName;
import java.util.Date;
import java.util.List;

import io.realm.RealmObject;

public class Results extends RealmObject {

    @SerializedName("opensearch:Query")
    private OpenSearchQuery openSearchQuery;

    @SerializedName("opensearch:totalResults")
    private String totalResults;

    @SerializedName("albummatches")
    private AlbumMatches albumMatches;

    @SerializedName("localCacheDate")
    private Date localCacheDate;

    public String getTotalResults() {
        return totalResults;
    }

    public AlbumMatches getAlbumMatches() {
        return albumMatches;
    }

    public OpenSearchQuery getOpenSearchQuery() {
        return openSearchQuery;
    }

    public void addAlbums(List<AlbumSimple> newAlbums) {
        if (albumMatches != null)
            albumMatches.addAlbums(newAlbums);
    }
}
