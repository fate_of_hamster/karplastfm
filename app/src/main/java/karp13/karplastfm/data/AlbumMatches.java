package karp13.karplastfm.data;

import com.google.gson.annotations.SerializedName;
import java.util.List;
import io.realm.RealmList;
import io.realm.RealmObject;

public class AlbumMatches extends RealmObject {

    @SerializedName("album")
    private RealmList<AlbumSimple> album;

    public RealmList<AlbumSimple> getAlbums() {
        return album;
    }

    public void addAlbums(List<AlbumSimple> newAlbums) {
        album.addAll(newAlbums);
    }
}
