package karp13.karplastfm.data;

import com.google.gson.annotations.SerializedName;
import io.realm.RealmList;
import io.realm.RealmObject;

// делаю отдельный класс для альбомов в списке результатов поиска, т.к. иначе realm кэширует их в ту же табличку, где и основные альбомы с полной информацией
public class AlbumSimple extends RealmObject {

    private static final String FIELD_NAME = "name";
    private static final String FIELD_ARTIST = "artist";
    private static final String FIELD_MBID = "mbid";

    @SerializedName(FIELD_NAME)
    private String name;

    @SerializedName(FIELD_ARTIST)
    private String artist;

    @SerializedName(FIELD_MBID)
    private String mbid;

    @SerializedName("url")
    private String url;

    @SerializedName("image")
    private RealmList<Image> images;


    public RealmList<Image> getImages() {
        return images;
    }

    public String getName() {
        return name;
    }

    public String getArtist() {
        return artist;
    }

    public String getMbid() {
        return mbid;
    }

    public String getUrl() {
        return url;
    }

    public String getLinkOnSmallImage() {
        if (images != null) {
            String small = null;
            String medium = null;
            String large = null;

            for (Image image : images) {
                if (image.isSmall())
                    small = image.getText();
                else if (image.isMedium())
                    medium = image.getText();
                else if (image.isLarge())
                    large = image.getText();
            }

            return (large != null) ? large : // приоритет отдаём размеру large, т.к. small уж больно маленький для нас
                    (medium != null) ? medium :
                            small;
        }
        else
            return null;
    }
}
