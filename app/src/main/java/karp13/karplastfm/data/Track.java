package karp13.karplastfm.data;

import com.google.gson.annotations.SerializedName;

import java.util.concurrent.TimeUnit;

import io.realm.RealmObject;

public class Track extends RealmObject {

    @SerializedName("name")
    private String name;

    @SerializedName("url")
    private String url;

    @SerializedName("duration")
    private String duration;

    @SerializedName("artist")
    private Artist artist;

    public String getName() {
        return name;
    }

    public String getUrl() {
        return url;
    }

    public String getDuration() {
        return duration;
    }

    public Artist getArtist() {
        return artist;
    }

    public String getDurationFormat() { // вероятно есть способ аккуратнее, но пока нашёл первое что было под рукой
        if (duration != null) {
            int durationSec = Integer.parseInt(duration);
            return String.format("%02d:%02d",
                            TimeUnit.SECONDS.toMinutes(durationSec),
                            TimeUnit.SECONDS.toSeconds(durationSec % 60));
        }
        else
            return null;
    }
}
