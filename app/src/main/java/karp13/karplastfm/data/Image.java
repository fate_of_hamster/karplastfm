package karp13.karplastfm.data;

import com.google.gson.annotations.SerializedName;
import io.realm.RealmObject;

public class Image extends RealmObject {

    public static final String SIZE_SMALL = "small";
    public static final String SIZE_MEDIUM = "medium";
    public static final String SIZE_LARGE = "large";
    public static final String SIZE_EXTRALARGE = "extralarge";
    public static final String SIZE_MEGA = "mega";

    @SerializedName("#text")
    private String text;

    @SerializedName("size")
    private String size;

    public String getText() {
        return text;
    }

    public boolean isSmall() {
        return size.equals(SIZE_SMALL);
    }
    public boolean isMedium() {
        return size.equals(SIZE_MEDIUM);
    }
    public boolean isLarge() {
        return size.equals(SIZE_LARGE);
    }
    public boolean isExtraLarge() {
        return size.equals(SIZE_EXTRALARGE);
    }
    public boolean isMega() {
        return size.equals(SIZE_MEGA);
    }
}
